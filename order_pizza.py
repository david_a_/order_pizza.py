'''

	this script orders pizza from dominos
	it uses their twitter easyorder functionality

'''

import ConfigParser
from random import randint
from twitter import *
from datetime import *
import time
import json


###########

class Pizza_Orderer(object):

    # All we need is a working twitter session.
    def __init__(self, Twitter):
        self.twitter_session = Twitter
        self.order_string = 'Scripting a @dominos #easyOrder with Python!'

    # A pizza order is started by posting a status containing both '@dominos' and '#easyOrder'
    def start_pizza_order(self):
        now = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        t_status = self.order_string + ' - ' + str(now)
        self.twitter_session.statuses.update(status=t_status)


    # After you post the tweet, dominos sends you a direct message to confirm your order
    def check_for_response(self):
            i=0
            for msg in reversed(t.direct_messages()):

                    # We want to see if dominos has sent us a confirmation message within 3 minutes ago

                    now = datetime.utcnow()
                    age = now - datetime.strptime(msg['created_at'],"%a %b %d %H:%M:%S +0000 %Y")
                    text = msg['text']
                    is_recent = 1 if age.seconds < 1800 else 0
                    sender = msg['sender_screen_name']

                    # If its recent and from dominos and start with "To complete your Easy Order",
                    # send dominos a DM to confirm

                    if is_recent==1 and sender == 'dominos' and text.startswith('To complete your Easy Order'):
                            print_and_log('Found a confirmation message.', log_file)
                            return True

                    i+=1

            return False


    # Send a confirmation message to dominos to order the pizza
    # THIS WILL ACTUALLY ORDER THE PIZZA
    def send_confirmation(self):
        self.twitter_session.direct_messages.new(user="dominos", text="CONFIRM")


    # Sends a NON-REAL confirmation message back to dominos user
    # Use for test purposes
    def send_confirmation_dev(self):
        self.twitter_session.direct_messages.new(user="dominos", text="CORRRRNFIRM")


    # After you confirm, they send a message with order-placed info
    def check_for_success(self):
            i=0
            for msg in reversed(t.direct_messages()):

                    # We want to see if dominos has sent us a confirmation message within 3 minutes ago

                    now = datetime.utcnow()
                    age = now - datetime.strptime(msg['created_at'],"%a %b %d %H:%M:%S +0000 %Y")
                    text = msg['text']
                    is_recent = 1 if age.seconds < 1800 else 0
                    sender = msg['sender_screen_name']

                    # If its recent and from dominos and starts with "Delivery Order place",
                    # then it probably worked

                    if is_recent==1 and sender == 'dominos' and text.startswith('Delivery Order placed'):
                            print_and_log('SUCCESS?!', log_file)
                            print_and_log(text, log_file)
                            return True

                    i+=1

            return False



# Define a print+log function
def print_and_log(message, log_file):
    now = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    print str(now) + ': ' + message
    with open(log_file, "a") as logfile:
        logfile.write(str(now) + ': ' + message + '\n')


    ### Begin Execution

# Set Log File
log_file = 'eo.log'

# Get Configuration File
config_file = 'eo.conf'
config = ConfigParser.ConfigParser()
config.read(config_file)

# Get Twitter Credentials from config_file
access_token = config.get('TwitterCredentials','access_token')
access_token_secret = config.get('TwitterCredentials','access_token_secret')
consumer_key = config.get('TwitterCredentials','consumer_key')
consumer_secret = config.get('TwitterCredentials','consumer_secret')


# Create a Twitter Session
print_and_log('Authenticating to Twitter...', log_file)
t = Twitter(auth = OAuth(access_token, access_token_secret, consumer_key, consumer_secret))

# Use the twitter session to create a Pizza_Orderer object
p = Pizza_Orderer(t)


# Start the pizza order
print_and_log('Starting the ordering process...', log_file)
p.start_pizza_order()


# We need to wait and check for the confirmation direct_message
# Let's check 12 times in 3 minutes
print_and_log('Checking for response every 15 seconds for 3 minutes...', log_file)
for i in range(12):

    # Check inbox for the direct_mail
	print_and_log(str(i) + ': Checking now', log_file)
	if p.check_for_response() == True:
		print_and_log('Found! Sending confirmation back...', log_file)
		# confirmed = 1
        # If it's found, we need to send direct_message that says "CONFIRM"
        # in the text to confirm the order and then break out of the loop
        # CHANGE to "p.send_confirmation()" TO ORDER PIZZA FOR REAL"
		p.send_confirmation_dev()
		break
	else:
        # otherwise wait
		time.sleep(15)



# We need to wait and check for the order direct_message
# Let's check 6 times in 18 sec
print_and_log('Checking for order conf. direct_message', log_file)
for i in range(6):

    # Check inbox for the direct_mail
    print_and_log(str(i) + ': Checking for Success...', log_file)
    if p.check_for_success() == True:
        confirmed = 1
        break
    else:
        # otherwise wait
        time.sleep(3)


if confirmed==1:
    # Success!
	print_and_log('Your pizza is on the way!', log_file)
else:
    # Confirmation message never came
	print_and_log('Whelp, we tried.', log_file)

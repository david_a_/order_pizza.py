#order_pizza.py



This script orders pizza to your house when you run it.

It uses dominos' easyorder functionality and twitter's API to function


## Instructions: ##

1. Get twitter account
	[https://twitter.com](Link URL)

2. Set up dominos anyware – it will use your easyOrder order and deliver you that

	[https://anyware.dominos.com/](Link URL)

3. Clone repo

	```
	git clone https://bitbucket.org/david_a_/order_pizza.py.git
	```

4. Install configparser and twitter libraries

	```
	pip install twitter
	```
	```
	pip install configparser
	```

5. Create an app and retrieve the tokens and keys (sorry, you can't use mine yet)

	[https://apps.twitter.com](Link URL)

6. Enter tokens and keys into eo.conf

	```
	nano eo.conf
	```

7. Test Run: run order_pizza.py

	```
	python order_pizza.py
	```

8. Real Deal: edit order_pizza.py – 
	Change "p.send_confirmation_dev()" to "p.send_confirmation()" to order pizza for realz

9. Order Pizza For Realz: run order_pizza.py

	```
	python order_pizza.py
	```

10. Enjoy (hopefully it worked)